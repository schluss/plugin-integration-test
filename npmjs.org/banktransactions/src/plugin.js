// yes it is possible to load other npm libraries inside a plugin
var ipfs = require('ipfs-minime');

var config = {
	
	viewpath : '/app_modules/banktransactions/assets/views/',
	
};

module.exports = {
	
	init: function(bridge)
	{
		console.log('called: init inside plugin');
		
		/* 
		todo: subrouting here: then we are able to show different views so:
		- /plugin/banktransactions/selectbank --> goes to select a bank view 
		- /plugin/banktransactions/dosomethingelse/1 --> goes to a different view
		*/

		
		// publish an event to the main app
		bridge.events.publish('testEvent', 'Event: I\'m shouting to app form banktransaction plugin');

		// listen to a event that is fired in the 
		bridge.events.subscribe('testEvent2', function(somedata){
			console.log('Event: I\'m listening to testEvent2 from app and recieved : ' + somedata);
		});
		
		// add routes:
		
		bridge.events.publish('onRegisterRoutes', [
					['get', '#/testroute', function(e){
						e.partial(config.viewpath + 'selectbank.tpl', null, function(t){
							console.log('selectbank view is shown');
						});
					}],
					['get', '#/returndata', function(e){
						
						// we processed the returned data here, now send it back to the main app to do the finalizing (storing / crypting etc)				
						bridge.events.publish('onFinalizeRequest',['this','is','a','testdata','structure']);
					}]					
					// more routes here
				]);
		
		// redirect to the newly added route
		window.location.href = '#/testroute';		
	}
};