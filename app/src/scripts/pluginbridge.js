module.exports = {
	
	doTest : function(){
		
		alert('doTest()');
		
	},
	
	initPlugin : function(pluginname, attrs){
		
		import (
		/* webpackChunkName: "plugin." */	
		`../../app_modules/node_modules/${pluginname}/src/plugin.js`
		).then(x=>{
			x.init(attrs);  // evt extra params in init function meesturen?
		}).catch(error => {
			//reject(error);
			console.log(error);
		});
	}
	
};