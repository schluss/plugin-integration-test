var bridge = require('./pluginbridge.js');
var pubsub = require('./pubsub.js');


window.$ = global.jQuery = require('jquery');
var app = $.sammy = require('sammy');

console.log('APP loaded!');

// Listen to events fired elsewhere (for example fired from the plugin)
pubsub.subscribe('testEvent', function(recievedData){
	console.log(recievedData);
});




var app = $.sammy('#main', function () {
	
	this.get('#/', function(e){
		e.partial('views/home.tpl', function(e){
			
			document.getElementById('loadpluginlink').onclick = () => {
				bridge.initPlugin('banktransactions', { context : e, events : pubsub});
				
								
				// publish an event 'to' the plugin that is now initiated an listening 
				pubsub.publish('testEvent2', 'Ohlala');	

			};
			
		});
	});
	
	
	// Route that catches all routes starting with /plugin/, for example: '#/plugin/[nameofplugin]/test/1/2'
	this.get(/\#\/plugin\/(.*)/, function (e) {

		var splat = e.params['splat'];		
		var urlparams = splat[0].split('/');
		
		// urlparams[0] contains the pluginname, for example 'banktransactions'
		
		/* IDEA!: 
		 plugin could also be loaded programmatically:
		 this has an advantage as we are not 'routing an url' at the moment we are loading the plugin
		 so we can add new routes to sammy dynamically and then afterwards do the routing..
		
		*/
		
		bridge.initPlugin(urlparams[0], { context : e, events : pubsub});
		
		// publish an event 'to' the plugin that is now initiated an listening 
		pubsub.publish('testEvent2', 'Ohlala');		
		
	});	
	
});

// use pubsub mechanism to be able to dynamically add routes into SammyJS:
pubsub.subscribe("onRegisterRoutes", function(additionalRoutes){
	console.log('registering additional routes');
	app.mapRoutes(additionalRoutes);		
});	

// start a request
pubsub.subscribe("onInitRequest", function(requestObj){
	//console.log('registering additional routes');		
});	

// complete a request
pubsub.subscribe("onFinalizeRequest", function(requestObj){
	console.log('got data (from a request) from the plugin and store it in users locker');
	console.log('Data contents:');
	console.log(requestObj);
});	

$(function () {
	app.run('#/');
});