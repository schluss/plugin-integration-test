const path = require('path');
const CopyWebpackPlugin = require('copy-webpack-plugin');

module.exports = {
	target: 'web',
	mode: 'production',
	entry: {
		index: "./src/scripts/app.js"
	},

    output: {
		path: path.resolve(__dirname, 'dist/'),
		filename: '[name].bundle.js',
		chunkFilename: '[name].bundle.js',
    },
	module: {
	  rules: [{ 
			test: /\.js$/, 
			exclude: /node_modules/,
			loader: "babel-loader" 
			}
	  ]
	},
	plugins: [

		// copy static assets to dist folder
		new CopyWebpackPlugin([{

				from: './app_modules/node_modules',
				//to: 'app_modules/',
				//test : /([^/]+)\/assets\/views\/$/, // THIS IS NOT WORKING CORRECTLY YET.. ONLY WANT TO COPY /assets folder from every plugin
				//test : /.\/app_modules\/node_modules\/([^/]+)\/(assets)\/$/, // THIS IS NOT WORKING CORRECTLY YET.. ONLY WANT TO COPY /assets folder from every plugin
				to : 'app_modules',
				test : /assets\/$/,
			}
        ])
	  ],	
    devServer: {
        contentBase: 'dist'
    }
};